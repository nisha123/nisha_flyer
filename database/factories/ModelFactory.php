<?php



$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});



$factory->define(App\Flyer::class, function (Faker\Generator $faker) {
    return [
//        'street' => $faker->streetAddress,
         'user_id' => factory('App\User')->create()->id,
        'street' => 'iti circle',
        'city' => 'bikaner',
//        'city' => $faker->city,
        'zip' => 332001,
//        'zip' => $faker->postcode,
//        'state' => $faker->state,
        'state' => 'rajasthan',
        'country' => 'india',
//        'country' => $faker->country,
        'price' => 10000 ,
//        'price' => $faker->numberBetween(10000 ,5000000),
        'description' => 'hello welcome to flyer ',
//        'description' => $faker->paragraphs(3),

    ];
});
