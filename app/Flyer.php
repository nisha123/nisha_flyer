<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flyer extends Model
{
    protected $fillable =[
      'street',
        'user_id',
        'city',
        'state',
        'country',
        'zip',
        'price',
        'description'


    ];

    public  static function LocatedAt($zip,$street)
//    public function scopeLocatedAt($query ,$zip,$street)
    {
        $street = str_replace('-','',$street);

        return static::where(compact('zip','street'))->firstOrFail();
//        return $query->where(compact('zip','street'));
    }


    public  function getPriceAttribute($price)
    {
        return '$'.number_format($price);
    }

    public  function addPhoto(photo $photo)
    {
          return $this->photos()->save($photo);
    }


    public  function photos()
    {
        return $this->hasMany('App\photo');
    }

    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public  function ownedBy(User $user)
    {
       return $this->user_id == $user->id;
    }


}
