<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;


class photo extends Model
{
    protected $table = 'flyer_photos';

    protected $fillable = ['path','name','flyer_id' ,'thumbnail_path'];

//    protected $file;
//
//    protected static function boot()
//    {
//
//        static::creating(function($photo)
//        {
////            dd($photo);
//           return $photo->Upload();
//
//        });
//    }

    protected $baseDir ='flyerpics/photos';


    public function flyer()
    {
        return $this->belongsTo('App\Flyer');
    }


//    public static function fromFile(UploadedFile $file)
//    {
//        $photo = new static;
//
//        $photo->file = $file;
//
//       return  $photo->fill([
//            'name' => $photo->fileName(),
//            'path' => $photo->filePath(),
//            'thumbnail_path' => $photo->thumbnailPath()
//        ]);
//
//    }

     public  function setNameAttribute($name)
     {
         $this->attributes['name'] = $name;
         $this->path = $this->baseDir().'/'.$name;
         $this->thumbnail_path = $this->baseDir().'/tn-'.$name;

     }

//    public function filePath()
//    {
//        return $this->baseDir().'/'.$this->fileName();
//    }


//    public function thumbnailPath()
//    {
//        return $this->baseDir().'/tn-'.$this->fileName();
//    }


    public function baseDir()
    {
//        return 'images/photos ';
        return 'flyerpics/photos';
    }

//    public static function named($name)
//    {
////        $photo = new static;
//        return (new static)->saveAs($name);
////        $file = $request->file('photo');
////        $name = time().$file->getClientOriginalName();
////        $photo->path = $photo->baseDir. '/'.$name;
//    }

//    protected  function saveAs($name)
//    {
//        $this->name = sprintf("%s-%s",time(),$name);
//        $this->path = sprintf("%s/%s",$this->baseDir,$this->name);
//        $this->thumbnail_path = sprintf("%s/tn-%s",$this->baseDir,$this->name);
//        return $this;
//
//
//    }


//    public function Upload()
//    {
//
//
//        $this->makeThumbnail();
//
//        return $this;
//
//    }


//    protected function makeThumbnail()
//    {
//
//        Image::make($this->path())
//            ->fit(200)
//            ->save($this->thumbnail_path());
//
//    }
}
