<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Routing\Controller;

use Auth;
use App\Http\Controllers\Flyer;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\AuthorizesUsers;
use App\Http\Requests\FlyerRequest;
use Illuminate\Http\UploadedFile;



class FlyresController extends Controller
{

    public function __construct()
        {
            $this->user = Auth::user();
//            dd('running flyer class construct');
            $this->middleware('auth',['except' => ['show']]);
        }

    public  function create()
        {
            return view('flyers.create');
        }



    public function store(FlyerRequest $request)
        {
//            App\Flyer::create($request->all())->first();
//                              dd($request->all());
            $flyer = $this->user->publish(new App\Flyer($request->all()));

            flash()->overlay('flyer successfully created!','good job');

              return redirect(flyer_path($flyer));
        }


    public  function show($zip , $street)
        {
            $flyer = App\Flyer::locatedAt($zip , $street);

            return view('flyers.show',compact('flyer'));

    //        return App\Flyer::where(compact('zip','street'))->first();
        }



//    protected function makePhoto(UploadedFile $file)
//    {
//        return Photo::named($file->getClientOriginalName())
//                        ->move($file);
//    }


}
