<?php
namespace App;

use Mockery as m ;
use App\AddPhotoToFlyer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Illuminate\Http\UploadedFile;


class AddPhotoToFlyerTest extends \TestCase
{

    function tests_it_Processes_a_form_to_add_a_photo_to_a_flyer()
    {
        $flyer = factory(Flyer::class)->create();

        $file = m::mock(UploadedFile::class, [
            'getClientOriginalName' => 'foo',
            'getClientOriginalExtension' => 'jpg'
        ]);

        $file->shouldReceive('move')
            ->once()
            ->with('flyerpics/photos', 'nowfoo.jpg');

        $thumbnail = m::mock(Thumbnail::class);

        $thumbnail->shouldReceive('make')
            ->once()
            ->with('flyerpics/photos/nowfoo.jpg', 'flyerpics/photos/tn-nowfoo.jpg');

        (new AddPhotoToFlyer($flyer, $file, $thumbnail))->save();

        $this->assertCount(1, $flyer->photos);
    }
}

    function time()
    {
        return 'now';
    }

    function sha1($path)
    {
        return $path;
    }
